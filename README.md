Роль deploy_monitoring
======================

Содержит только роль *deploy_monitoring*, которая устанавливает необходимые пакеты на целевой хост, копирует на него файл *docker-compose.yml* и запускает им приложения Prometheus, Grafana и Cadvisor.  

Ссылка на репозиторий:  
https://gitlab.com/AYuSuslov/monitoring-deploy-role-less-3.9

Целевой хост принадлежит группе *gcp_env_mon*. На нём устанавливаю докер и композ, стартую докер, копирую с управляющего хоста конфигурационный файл *prometheus.yml*, файл запуска *docker-compose.yml* и запускаю композ.

